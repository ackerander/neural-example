# Neural Example

Basic neural net example using mnist database and libcerebellum library.

## Dependencies

* libcerebellum
* libpng

## Quick Start

```console
$ make
```
