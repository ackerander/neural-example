CFLAGS=-std=c99 -pedantic -Wall -Wextra -O3 -flto

ifdef STATIC_CEREBELLUM
LD_CEREBELLUM=-l:libcerebellum.a -lm
else
LD_CEREBELLUM=-lcerebellum
endif

all: class test train

class: classify.c
	cc ${CFLAGS} ${FLAGS} -o $@ $< -lpng ${LD_CEREBELLUM}

test: test.c
	cc ${CFLAGS} ${FLAGS} -o $@ $< ${LD_CEREBELLUM}

train: train.c
	cc ${CFLAGS} ${FLAGS} -o $@ $< ${LD_CEREBELLUM}
