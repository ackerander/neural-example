#include <stdio.h>
#include <cerebellum.h>
#include <time.h>
#include <stdint.h>
#include <math.h>

#define LYRS 0, 40, 20, 10
#define NLYRS 4
#define R 0.25, 0.15, 0.1
#define EPS 20, 20, 10
#define N 3
#define IMGS "data/train-images-idx3-ubyte"
#define LBLS "data/train-labels-idx1-ubyte"

static net_t net;
static double *in;
static uint8_t *out;
static size_t samples;
static double outbuff[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

uint32_t
smolend(uint32_t val)
{
	val = ((val << 8) & 0xFF00FF00 ) | ((val >> 8) & 0xFF00FF ); 
	return (val << 16) | (val >> 16);
}
/*
void
standardize(double *arr, size_t len)
{
	double mean = 0, std = 0;

	for (size_t i = 0; i < len; ++i)
		mean += arr[i];
	mean /= len;
	for (size_t i = 0; i < len; ++i)
		std += (arr[i] - mean) * (arr[i] - mean);
	std = sqrt(std/len);
	for (size_t i = 0; i < len; ++i)
		arr[i] = (arr[i] - mean)/std;
}
*/
int
openFiles()
{
	uint32_t n, m;
	FILE *imgs;
	FILE *lbls;
	uint8_t *buffer;

	imgs = fopen(IMGS, "rb");
	lbls = fopen(LBLS, "rb");
	fseek(imgs, 4, SEEK_SET);
	fseek(lbls, 8, SEEK_SET);

	fread(&n, 4, 1, imgs);
	samples = smolend(n);
	fread(&n, 4, 1, imgs);
	n = smolend(n);
	fread(&m, 4, 1, imgs);
	m = smolend(m);
	net.layers[0] = n*m;
	printf("Allocating memory\n");
	in = malloc(n*m*samples*sizeof(double));
	buffer = malloc(n*m);
	out = malloc(samples);

	printf("Reading from files\n");
	for (size_t i = 0; i < samples; ++i) {
		fread(buffer, n*m, 1, imgs);
		for (size_t j = 0; j < n*m; ++j)
			in[i*n*m + j] = (double)buffer[j]/0xFF;
//		standardize(in + i*n*m, n*m);
	}
	fread(out, 1, samples, lbls);

	fclose(imgs);
	fclose(lbls);
	free(buffer);
	return 0;
}

uint8_t
maxidx(double *arr)
{
	uint8_t idx = 0;
	double max = -1;

	for (short i = 0; i < 10; ++i) {
		if (arr[i] > max) {
			max = arr[i];
			idx = i;
		}
	}
	return idx;
}

double
initWs(size_t i)
{
	double w = 0.7*rand()/(double)RAND_MAX;
	return (i & 1) ? -w : w;
}

double
initBs(size_t i)
{
	double b = rand()/(double)RAND_MAX;
	return (i & 1) ? -b : b;
}

int
main(int argc, char **argv)
{
	size_t lyrs[NLYRS] = {LYRS}, eps[N] = {EPS}, acc = 0;
	double rs[N] = {R};

	net.layers = lyrs;
	net.nlayers = NLYRS;
	openFiles();

	srand(time(0));
	initNet(&net, initWs, initBs);
	printf("Training network");
	for (size_t i = 0; i < N; ++i) {
		net.rate = rs[i];
		printf("\nStage %ld/%d  0%%", (i + 1), N);
		fflush(stdout);
		for (size_t j = 0; j < eps[i]; ++j) {
			for (size_t k = 0; k < samples; ++k) {
				outbuff[(int)out[k]] = 1.0;
				feedforward(&net, in + k*net.layers[0]);
				train(&net, in + k*net.layers[0], outbuff);
				outbuff[(int)out[k]] = 0.0;
			}
			printf("\b\b\b%2ld%%", 100*(j + 1)/eps[i]);
			fflush(stdout);
		}
	}
	printf("\n");

	for (size_t i = 0; i < samples; ++i)
		acc += maxidx(feedforward(&net, in + i*net.layers[0])) == out[i];
	printf("Final success rate: %.3f%%\n", 100*(double)acc/samples);
	if (argc >= 2)
		saveNet(&net, argv[1]);

	delNet(&net);
	free(in);
	free(out);
	return 0;
}
/*
		idx = 0;
		for (size_t i = 0; i < samples; ++i)
			idx += maxidx(feedforward(&net, in + i*net.layers[0])) == out[i];
		printf("Success rate: %.3f, %04.1f%%\n", 100*(double)idx/samples, (double)i/10);
*/
