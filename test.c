#include <stdio.h>
#include <cerebellum.h>
#include <time.h>
#include <stdint.h>
#include <math.h>

#define IMGS "data/t10k-images-idx3-ubyte"
#define LBLS "data/t10k-labels-idx1-ubyte"

static net_t net;
static double *in;
static uint8_t *out;
static size_t samples;

uint32_t
smolend(uint32_t val)
{
	val = ((val << 8) & 0xFF00FF00 ) | ((val >> 8) & 0xFF00FF ); 
	return (val << 16) | (val >> 16);
}

/*
void
standardize(double *arr, size_t len)
{
	double mean = 0, std = 0;

	for (size_t i = 0; i < len; ++i)
		mean += arr[i];
	mean /= len;
	for (size_t i = 0; i < len; ++i)
		std += (arr[i] - mean) * (arr[i] - mean);
	std = sqrt(std/len);
	for (size_t i = 0; i < len; ++i)
		arr[i] = (arr[i] - mean)/std;
}
*/

int
openFiles()
{
	uint32_t n, m;
	FILE *imgs;
	FILE *lbls;
	uint8_t *buffer;

	imgs = fopen(IMGS, "rb");
	lbls = fopen(LBLS, "rb");
	fseek(imgs, 4, SEEK_SET);
	fseek(lbls, 8, SEEK_SET);

	fread(&n, 4, 1, imgs);
	samples = smolend(n);
	fread(&n, 4, 1, imgs);
	n = smolend(n);
	fread(&m, 4, 1, imgs);
	m = smolend(m);
	net.layers[0] = n*m;
	printf("Allocating memory\n");
	in = malloc(n*m*samples*sizeof(double));
	buffer = malloc(n*m);
	out = malloc(samples);

	printf("Reading from files\n");
	for (size_t i = 0; i < samples; ++i) {
		fread(buffer, n*m, 1, imgs);
		for (size_t j = 0; j < n*m; ++j)
			in[i*n*m + j] = (double)buffer[j]/0xFF;
//		standardize(in + i*n*m, n*m);
	}
	fread(out, 1, samples, lbls);

	fclose(imgs);
	fclose(lbls);
	free(buffer);
	return 0;
}

uint8_t
maxidx(double *arr)
{
	uint8_t idx = 0;
	double max = -1;

	for (short i = 0; i < 10; ++i) {
		if (arr[i] > max) {
			max = arr[i];
			idx = i;
		}
	}
	return idx;
}

int
main(int argc, char **argv)
{
	size_t acc = 0;

	if (argc < 2)
		return 1;
	netFromFile(&net, argv[1]);
	openFiles();
	for (size_t i = 0; i < samples; ++i)
		acc += maxidx(feedforward(&net, in + i*net.layers[0])) == out[i];
	printf("Success rate: %.3f%%\n", 100*(double)acc/samples);

	free(net.layers);
	delNet(&net);
	free(in);
	free(out);
	return 0;
}
