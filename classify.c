#include <cerebellum.h>
#include <png.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

static net_t net;
static double *in;

int
openFile(const char *const restrict fn)
{
	FILE *fp;
	png_structp pngp;
	png_infop info;
	png_uint_32 width, height;
	int bit_depth, color_type, interlace_method, compression_method,
		filter_method;
	png_bytepp rows;

	fp = fopen(fn, "rb");
	if (!fp)
		return -1;

	pngp = png_create_read_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);
	if (!pngp)
		return -2;

	info = png_create_info_struct(pngp);
	if (!info)
		return -3;

	png_init_io(pngp, fp);
	png_read_png(pngp, info, 0, 0);
	png_get_IHDR (pngp, info, &width, &height, &bit_depth,
		&color_type, &interlace_method, &compression_method,
		&filter_method);
	if (width * height != net.layers[0])
		return -4;
	rows = png_get_rows (pngp, info);
	in = malloc(width*height*sizeof(double));

	for (size_t i = 0; i < height; ++i) {
		for (size_t j = 0; j < width; ++j)
			in[i*width + j] = 1 - (double)(rows[i][3*j] + rows[i][3*j + 1] + rows[i][3*j + 2])/(3*0xFF);
	}
//	standardize(in, width*height);
	fclose(fp);
	return 0;
}

short
maxidx(const double *const restrict arr)
{
	short idx = 0;
	double max = -1;

	for (short i = 0; i < 10; ++i) {
		if (arr[i] > max) {
			max = arr[i];
			idx = i;
		}
	}
	return idx;
}

int
main(int argc, char **argv)
{
	if (argc != 3)
		return EXIT_FAILURE;
	netFromFile(&net, argv[1]);
	switch (openFile(argv[2])) {
	case -1:
		fprintf(stderr, "Cannot open '%s' : %s\n", argv[3], strerror(errno));
		return EXIT_FAILURE;
		break;
	case -2:
		fprintf(stderr, "Cannot crate PNG structure\n");
		return EXIT_FAILURE;
		break;
	case -3:
		fprintf(stderr, "Cannot crate PNG info\n");
		return EXIT_FAILURE;
		break;
	case -4:
		fprintf(stderr, "Dimension mismatch\n");
		return EXIT_FAILURE;
		break;
	}
	printf("That's a %d\n", maxidx(feedforward(&net, in)));
	free(in);
	free(net.layers);
	delNet(&net);
	return 0;
}
